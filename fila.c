#include <stdio.h>
#include <stdlib.h>

#include "fila.h"

char filaNome[] = "Lista de compras";

int tam;

int fila()
{
 node *FILA = (node *) malloc(sizeof(node));
 if(!FILA){
  printf("Sem memoria disponivel!\n");
  exit(1);
 }else{
 inicia(FILA);
 int opt;

 do{
  opt=menu();
  opcao(FILA,opt);
 }while(opt);

 free(FILA);
 return 0;
 }
}

int menu()
{
 int opt;
 printf("\t\t Escolha a opcao");
 printf ("\n|----------------------------------------------------|\n");
 printf("  0.\t\t Sair");
 printf ("\n|----------------------------------------------------|\n");
 printf("  1.\t\t Zerar lista de compras");
 printf ("\n|----------------------------------------------------|\n");
 printf("  2.\t\t Exibir fila");
 printf ("\n|----------------------------------------------------|\n");
 printf("  3.\t\t Adicionar Elemento na Fila");
 printf ("\n|----------------------------------------------------|\n");
 printf("  4.\t\t Retirar Elemento da Fila");
 printf ("\n|----------------------------------------------------|\n");
 printf("  Opcao: "); scanf("%d", &opt);
 printf ("\n|----------------------------------------------------|\n");

 return opt;
}

void opcao(node *FILA, int op)
{
 node *tmp;
 switch(op){
  case 0:
   libera(FILA);
   break;

  case 1:
   libera(FILA);
   inicia(FILA);
   break;

  case 2:
   exibe(FILA);
   break;

  case 3:
   insere(FILA);
   break;

  case 4:
   tmp= retira(FILA);
   if(tmp != NULL){
    printf("Retirado: %3d\n\n", tmp->num);
    libera(tmp);
   }
   break;

  default:
   printf("Comando invalido\n\n");
 }
}

void inicia(node *FILA)
{
 FILA->prox = NULL;
 tam=0;
}

int vazia(node *FILA)
{
 if(FILA->prox == NULL)
  return 1;
 else
  return 0;
}

node *aloca()
{
 node *novo=(node *) malloc(sizeof(node));
 if(!novo){
      printf("Sem memoria disponivel!\n");
      exit(1);
 }else{
      printf("Novo elemento: "); scanf("%s", &novo->num);
      return novo;
 }
}

void insere(node *FILA)
{
 node *novo=aloca();
 novo->prox = NULL;

 if(vazia(FILA))
  FILA->prox=novo;
 else{
  node *tmp = FILA->prox;

  while(tmp->prox != NULL)
   tmp = tmp->prox;

  tmp->prox = novo;
 }
 tam++;
}


node *retira(node *FILA)
{
 if(FILA->prox == NULL){
  printf("Fila ja esta vazia\n");
  return NULL;
 }else{
  node *tmp = FILA->prox;
  FILA->prox = tmp->prox;
  tam--;
  return tmp;
 }

}


void exibe(node *FILA)
{
 if(vazia(FILA)){
  printf("Fila vazia!\n\n");
  return ;
 }

 node *tmp;
 tmp = FILA->prox;
 printf("Fila :");
 while( tmp != NULL){
  printf("%s", tmp->num);
  tmp = tmp->prox;
 }
 printf("\n        ");
 int count;
 for(count=0 ; count < tam ; count++)
  printf("  ^  ");
 printf("\nOrdem:");
 for(count=0 ; count < tam ; count++)
  printf("%5d", count+1);


 printf("\n\n");
}

void libera(node *FILA)
{
 if(vazia(FILA)){
  node *proxNode,
     *atual;

  atual = FILA->prox;
  while(atual != NULL){
   proxNode = atual->prox;
   free(atual);
   atual = proxNode;
  }
 }
}

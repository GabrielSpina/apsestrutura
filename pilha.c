#include <stdio.h>
#include <stdlib.h>
#include<string.h>

#include "pilha.h"

char pilhaNome[] = "Editor";

void pilha(){
    printf("Arquivo de texto\ninsira 'volta' em uma linha para voltar ou '0' para encerrar!\n\n");

    Pilha pilha;
    iniciarPilha(& pilha);

    int run = 1;
    while(run){

        Celula * linha = push(&pilha);

        if(strcmp(linha->item, "0") == 0){
            //Remove da lista a ordem de parar
            remover(&pilha, peek(&pilha));

            run = 0;
            printf("Fim da edição de texto!\n");

        }else if(strcmp(linha->item, "volta") == 0){
            //Remove da lista a ordem de voltar
            remover(&pilha, peek(&pilha));

            //Remove a ultima linha
            remover(&pilha, peek(&pilha));

            imprimirPilha(&pilha);

        }
    }

    printf("\n\nSeu arquivo ficou:\n");
    imprimirPilha(&pilha);
}

void iniciarPilha (Pilha * pilha){
	pilha->inicio = NULL;
	pilha->fim = NULL;
}

Celula * push(Pilha *pilha){
    Celula *p;
	p = (Celula *) malloc( sizeof(Celula));

    setbuf(stdin, NULL);
    scanf("%[^\n]s", &p->item);
    setbuf(stdin, NULL);

	p->prox = pilha->inicio;
	p->ant = NULL;
	if ( pilha->inicio != NULL){
		pilha->inicio->ant = p;
	}
	pilha->inicio = p;
	if (pilha->fim == NULL){
		pilha->fim = p;
	}

	return p;

}

Celula * peek(Pilha * pilha){
    Celula *p;
	p = (Celula *) malloc( sizeof(Celula));
	p = pilha->inicio;
	return p;
}

void remover(Pilha * pilha, Celula * p ) {
	if ( p==NULL ) return;
	if ( p->ant != NULL ) {
		p->ant->prox = p->prox;
	}
	if ( p->prox != NULL ) {
		p->prox->ant = p->ant;
	}
	if ( p == pilha->inicio ) {
		pilha->inicio = p->prox;
	}
	if ( p == pilha->fim ) {
		pilha->fim = p->ant;
	}
	free(p);
}

void imprimirPilha(Pilha * pilha) {

	Celula *p;
	for (p= pilha->fim; p != NULL; p= p->ant){
		printf("%s\n", p->item);
	}
}

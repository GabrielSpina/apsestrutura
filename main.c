#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#include "fila.h"
#include "pilha.h"


int main(){
    setlocale(LC_ALL, NULL);
    int run = 1;
    int op;

    printf("Trabalho de APS Estrutura de dados\n\n");
    while(run){
        printf("\nEscolho um dos numeros para acessar a aplicação:\n1 -> %s\n2 -> %s\n0 -> Encerrar\n", filaNome, pilhaNome);
        scanf("%d",&op);

        switch(op){
            case 1:
                fila();
                break;
            case 2:
                pilha();
                break;
            case 0:
                run = 0;
                break;
            default:
                printf("O valor digitado não é uma entrada valida!\n\n");

        }
    }
    printf("Encerrando execução!\n\n");
    return 0;
}


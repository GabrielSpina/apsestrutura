#ifndef PILHA_H
#define PILHA_H

extern char pilhaNome[];

typedef struct sCelulaD {
	char item[255];
	struct sCelulaD *prox;
	struct sCelulaD *ant;
} Celula;


typedef struct sPilha {
	Celula * inicio;
	Celula * fim;
} Pilha;

void pilha();
void iniciarPilha (Pilha * pilha);
Celula * push(Pilha *pilha);
Celula * peek(Pilha * pilha);
void remover(Pilha * pilha, Celula * p );
void imprimirPilha (Pilha * pilha);

#endif

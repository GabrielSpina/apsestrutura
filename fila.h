#ifndef FILA_H
#define FILA_H

extern char filaNome[];

typedef struct sNode{
 char num[50];
 struct Node *prox;
} node;

int menu();
void opcao(node *FILA, int op);
void inicia(node *FILA);
int vazia(node *FILA);
node *aloca();
void insere(node *FILA);
node *retira(node *FILA);
void exibe(node *FILA);
void libera(node *FILA);

#endif
